using Amazon;
using Amazon.Rekognition;
using Amazon.Runtime;
using Amazon.Rekognition.Model;
using Image = Amazon.Rekognition.Model.Image;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing.Imaging;

namespace ReconocimientoFacialIA
{
    public partial class Form1 : Form
    {
        private bool isCapturing = false;
        private VideoCaptureDevice videoSource = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void captuarar1_Click(object sender, EventArgs e)
        {
            if (!isCapturing) // Comprobar si el bot�n est� en el estado de "captura"
            {
                // Buscar la c�mara disponible
                FilterInfoCollection devices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (devices.Count == 0)
                {
                    MessageBox.Show("No se ha detectado ninguna c�mara conectada.");
                    return;
                }

                // Conectar a la c�mara
                videoSource = new VideoCaptureDevice(devices[0].MonikerString);
                videoSource.Start();
                videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame);

                isCapturing = true; // Cambiar el estado del bot�n a "captura de video"
                captuarar1.Text = "Detener"; // Cambiar el texto del bot�n a "Detener"
            }
            else // El bot�n est� en el estado de "detener"
            {
                // Detener la captura de video
                videoSource.SignalToStop();
                videoSource.NewFrame -= new NewFrameEventHandler(video_NewFrame);
                videoSource = null;

                // Obtener la imagen actual del pictureBox
                Bitmap image = (Bitmap)pictureBox1.Image;

                // Guardar la imagen en la ruta especificada
                string savePath = @"C:\Symetry\Facial2\images\imagen1.jpg";
                image.Save(savePath, ImageFormat.Jpeg);

                isCapturing = false; // Cambiar el estado del bot�n a "captura de imagen"
                captuarar1.Text = "Capturar"; // Cambiar el texto del bot�n a "Capturar"
            }

        }

        private void video_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            // Obtener el cuadro actual del video
            Bitmap frame = (Bitmap)eventArgs.Frame.Clone();

            // Mostrar el cuadro en el objeto PictureBox
            pictureBox1.Image = frame;

        }

        private void video_NewFrame_2(object sender, NewFrameEventArgs eventArgs)
        {
            // Obtener el cuadro actual del video
            Bitmap frame = (Bitmap)eventArgs.Frame.Clone();

            // Mostrar el cuadro en el objeto PictureBox
            pictureBox2.Image = frame;
        }
            private void capturar2_Click(object sender, EventArgs e)
        {
            if (!isCapturing) // Comprobar si el bot�n est� en el estado de "captura"
            {
                // Buscar la c�mara disponible
                FilterInfoCollection devices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
                if (devices.Count == 0)
                {
                    MessageBox.Show("No se ha detectado ninguna c�mara conectada.");
                    return;
                }

                // Conectar a la c�mara
                videoSource = new VideoCaptureDevice(devices[0].MonikerString);
                videoSource.Start();
                videoSource.NewFrame += new NewFrameEventHandler(video_NewFrame_2);

                isCapturing = true; // Cambiar el estado del bot�n a "captura de video"
                capturar2.Text = "Detener"; // Cambiar el texto del bot�n a "Detener"
            }
            else // El bot�n est� en el estado de "detener"
            {
                // Detener la captura de video
                videoSource.SignalToStop();
                videoSource.NewFrame -= new NewFrameEventHandler(video_NewFrame_2);
                videoSource = null;

                // Obtener la imagen actual del pictureBox
                Bitmap image = (Bitmap)pictureBox1.Image;

                // Guardar la imagen en la ruta especificada
                string savePath = @"C:\Symetry\Facial2\images\imagen2.jpg";
                image.Save(savePath, ImageFormat.Jpeg);

                isCapturing = false; // Cambiar el estado del bot�n a "captura de imagen"
                capturar2.Text = "Capturar"; // Cambiar el texto del bot�n a "Capturar"
            }

            //// return targetImage;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        
             
        }

      

        private async void comparar_Click(object sender, EventArgs e)
        {
            try {
            string accessKey = "AKIAZ4PAXRM72ONKIB5N";
            string accessKeySecret = "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";
            var awsCredentials = new BasicAWSCredentials(accessKey, accessKeySecret);
            var rekognitionClient = new AmazonRekognitionClient(awsCredentials, RegionEndpoint.USEast1);

            //Images
            var sourceImage = new Image { Bytes = new MemoryStream(File.ReadAllBytes(@"C:\Symetry\Facial2\images\imagen1.jpg")) };

            var targetImage = new Image { Bytes = new MemoryStream(File.ReadAllBytes(@"C:\Symetry\Facial2\images\imagen2.jpg")) };

            var compareFacesRequest = new CompareFacesRequest
            {
                SourceImage = sourceImage,
                TargetImage = targetImage,
                SimilarityThreshold = 70f, // Tolerancia de coincidencia
            };

            var  compareFacesResponse = await rekognitionClient.CompareFacesAsync(compareFacesRequest);

            if (compareFacesResponse.FaceMatches.Count > 0)
            {
                float similarity = compareFacesResponse.FaceMatches[0].Similarity;
                Console.WriteLine($"La similitud entre los rostros es de {similarity}%");
                MessageBox.Show($"La similitud entre los rostros es de {similarity}%");
            }
            else
            {
                Console.WriteLine("Los rostros no son similares");
                MessageBox.Show("Los rostros no son similares");

            }
            }
            catch
            {
                MessageBox.Show("Fallo en el request ");
            }
        }

        public async void facialRequest()
        {
           

        }

       
    }
}