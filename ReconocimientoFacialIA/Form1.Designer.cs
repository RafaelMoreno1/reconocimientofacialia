﻿namespace ReconocimientoFacialIA
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.captuarar1 = new System.Windows.Forms.Button();
            this.capturar2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.comparar = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // captuarar1
            // 
            this.captuarar1.Location = new System.Drawing.Point(239, 486);
            this.captuarar1.Name = "captuarar1";
            this.captuarar1.Size = new System.Drawing.Size(112, 34);
            this.captuarar1.TabIndex = 0;
            this.captuarar1.Text = "Capturar";
            this.captuarar1.UseVisualStyleBackColor = true;
            this.captuarar1.Click += new System.EventHandler(this.captuarar1_Click);
            // 
            // capturar2
            // 
            this.capturar2.Location = new System.Drawing.Point(1007, 486);
            this.capturar2.Name = "capturar2";
            this.capturar2.Size = new System.Drawing.Size(112, 34);
            this.capturar2.TabIndex = 1;
            this.capturar2.Text = "Capturar";
            this.capturar2.UseVisualStyleBackColor = true;
            this.capturar2.Click += new System.EventHandler(this.capturar2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(37, 97);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(523, 346);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(788, 97);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(504, 346);
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // comparar
            // 
            this.comparar.Location = new System.Drawing.Point(606, 462);
            this.comparar.Name = "comparar";
            this.comparar.Size = new System.Drawing.Size(112, 34);
            this.comparar.TabIndex = 4;
            this.comparar.Text = "Comparar";
            this.comparar.UseVisualStyleBackColor = true;
            this.comparar.Click += new System.EventHandler(this.comparar_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(487, 546);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(345, 163);
            this.treeView1.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1335, 747);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.comparar);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.capturar2);
            this.Controls.Add(this.captuarar1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Button captuarar1;
        private Button capturar2;
        private PictureBox pictureBox1;
        private PictureBox pictureBox2;
        private Button comparar;
        private TreeView treeView1;
    }
}